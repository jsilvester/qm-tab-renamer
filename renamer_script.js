// ==UserScript==
// @name         Change Page Titles
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include      *quantummetric.com*
// @grant        none
// ==/UserScript==

(function() {
    addPageTitles();
    window.addEventListener('hashchange', addPageTitles, !1);

    function addPageTitles() {
        var subName = window.location.host.split(".")[0];
        var hash = window.location.hash.split("?")[0].split("/");

        if (hash[1].length == 0) {
            document.querySelector("title").innerText = (subName + " | pulse").replace(/_/g," ").toUpperCase();
        }
          else if (hash[1] == "page_speed" || hash[1] == "api_speed"){
              document.querySelector("title").innerText = (subName + " | Performance | " + hash[1]).replace(/_/g," ").toUpperCase();
}
        else if (!!isNaN(hash[hash.length - 1]) && (hash[1] == "dimension" || hash[1] == "settings" || hash[1] == "profile" || hash[1] == "users")) {
            if(!!hash[3]&&(hash[2] == "event"||hash[2] == "error")){
            document.querySelector("title").innerText = (subName + " | " + hash[2] + " | " + hash[3]).replace(/_/g," ").toUpperCase();
            }
            else document.querySelector("title").innerText = (subName + " | " + hash[1] + " | " + hash[2]).replace(/_/g," ").toUpperCase();
        } else if (!!hash.length&&!isNaN(hash[hash.length - 1]) && !!document.querySelector(".replay-primary")) {
            document.querySelector("title").innerText = (subName + " | replay").replace(/_/g," ").toUpperCase();
        }
        else if (hash[1].indexOf("dashboards" > -1) && !!document.querySelector(".submenu-link.is-active") && !!document.querySelector(".submenu-link.is-active").innerText) {
            document.querySelector("title").innerText = (subName + " | " + document.querySelector(".submenu-link.is-active").innerText + " | " + hash[1]).replace(/_/g," ").toUpperCase();
        } else if (!!hash) {
            document.querySelector("title").innerText = (subName + " | " + hash[1]).replace(/_/g," ").toUpperCase();
        }
    }
})();
